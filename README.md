# README #

This is the repo for cLDA project. cLDA is an LDA extension that supports must-links and cannot-links.

### training file format ###
The training file is in json format. Please refer to 
***websail.clda.data.Dataset.java***, ***websail.clda.data.Document.java*** and ***websail.clda.util.FileHandler.java*** to see how it is generated.

### constraint file format ###

```
#!json

{"mustlink":[{"similar":123,"target":456"},"cannotlink":[{"dissimilar":589,"target":1212}]}
```



### How to run ###
* to compile the package, run: **mvn clean package**
* to train an LDA model, run: **java -cp target/clda-1.0-SNAPSHOT-jar-with-dependencies.jar websail.clda.LDAMain  --train-file ./3news/3news.data --num-topics 3 --num-iterations 1000 --constraint-file ./3news/3news.constraint.json
**