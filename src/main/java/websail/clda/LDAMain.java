package websail.clda;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import websail.clda.data.Dataset;
import websail.clda.util.FileHandler;
import websail.clda.data.ConstraintReader;
import websail.clda.data.ConstraintSet;
import websail.clda.model.LDA;
import websail.clda.model.Estimator;
import websail.clda.model.LDATopWords;

public class LDAMain {
	@Option(name="--train-file", usage="training file")
	private String trainfile;
	
	@Option(name="--constraint-file", usage="constraint file")
	private String constraintfile;
	
	@Option(name="--num-topics", usage="num of topics")
	private int topic;
	
	@Option(name="--num-iterations", usage="number of iterations")
	private int iteration;
	
	private static final int TOPWORDS = 20;
	
	public static void parseCLI(String[] args, Object obj){
		CmdLineParser parser = new CmdLineParser(obj);
		parser.setUsageWidth(80);
		try{
			parser.parseArgument(args);
		}catch( CmdLineException e ) {
			System.out.println(e.getMessage());
		}
	}
	
	public LDAMain(String[] args){
		parseCLI(args, this);
	}
	
	public static void main(String args[]) throws IOException{
		LDAMain app = new LDAMain(args);
		
		//load data
		Dataset data = FileHandler.load(app.trainfile, Dataset.class);

		//load constraint
		ConstraintSet cset = ConstraintReader.loadConstraint(app.constraintfile, data);

		//initial lda
		LDA lda = new LDA(app.topic, data, new Random());
		
		//train lda
		Estimator.estimate(lda, app.iteration, cset);
		
		//output lda
		LDATopWords.print(lda, TOPWORDS, false);
		FileHandler.save("/home/yi/workspace/clda/simplex/3news.result", data);
	}
}
