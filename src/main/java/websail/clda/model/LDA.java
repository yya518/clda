package websail.clda.model;

import java.util.Random;

import websail.clda.data.Dataset;
import websail.clda.data.Document;
import websail.clda.util.Utility;

public class LDA {
	Dataset data;
	
	public int K;
	public int W;
	public int D;
	
	public int[][] nwt, ndt;
	public int[] nt;
	public double[][] theta, phi, alpha;
	double beta = 0.01;
	
	Random rng;

	/**
	 * initial a LDA model from a trained data
	 */
	public LDA(int topic, Dataset data){
		this.data = data;
		K = topic;
		W = data.vocabSize();
		D = data.size();
		
		nwt = new int[W][K];
		nt = new int[K];
		ndt = new int[D][K];		
		theta = new double[D][K];
		phi = new double[K][W];
		computeCount();
	}
	
	/*
	 * initial a LDA model from scratch
	 */
	public LDA(int topic, Dataset data, Random rng){
		
		this.data = data;
		
		this.rng = rng;
		
		K = topic;
		W = data.vocabSize();
		D = data.size();
		
		nwt = new int[W][K];
		nt = new int[K];
		ndt = new int[D][K];		
		theta = new double[D][K];
		phi = new double[K][W];
		initAlpha();
		
		randomInitTopic();
		computeCount();
	}
	
	public void initAlpha(){
		alpha = new double[D][K];
		double uniform_value = 1.0; //may varies
		for(int d = 0; d < D; d++){
			for(int k = 0; k < K; k++)
				alpha[d][k] = uniform_value;
		}
	}
	
	//randomly initials topic samples
	public void randomInitTopic(){
		for(int index = 0; index < data.size(); index++){
			Document doc = data.get(index);
			doc.topics = new int[doc.words.length];
			for(int i = 0; i < doc.words.length; i++){
				 doc.topics[i] = rng.nextInt(K);
			}
		}
	}
	
	public void computeCount(){
		for(int index = 0; index < data.size(); index++){
			Document doc = data.get(index);
			int[] words = doc.words;
			int[] topics = doc.topics;
			for(int widx = 0; widx < words.length; widx++){
				int w = words[widx];
				int t = topics[widx];
				nwt[w][t]++;
				ndt[index][t]++;
				nt[t]++;
			}
		}
	}
	
	public void computePhi(){
		double Vbeta = W * beta;
		for(int k = 0 ; k < K; k++){
			double sum = 0.0;
			for(int w = 0 ; w < W; w++){
				phi[k][w] = (nwt[w][k] + beta)/(nt[k] + Vbeta);
				sum += phi[k][w];
			}
			for(int w = 0; w < W; w++)
				phi[k][w] /= sum;
		}
	}
	
	//recompute phi with other model's counts
	public void computePhi(LDA lda){
		double Vbeta = W * beta;
		for(int k = 0 ; k < K; k++){
			double sum = 0.0;
			for(int w = 0 ; w < W; w++){
				phi[k][w] = (nwt[w][k] + lda.nwt[w][k] + beta)/(nt[k] + lda.nt[k] + Vbeta);
				sum += phi[k][w];
			}
			for(int w = 0; w < W; w++)
				phi[k][w] /= sum;
		}
	}
	
	public void computeTheta(){
		for(int index = 0; index < data.size(); index++)
			compute_theta(index);
	}

	public double[] compute_theta(int index){
		for(int k = 0; k < K; k++)
			theta[index][k] = ndt[index][k] + alpha[index][k];
		Utility.normalize(theta[index]);
		data.documents[index].setTheta(theta[index]);
		return theta[index];
	}
}