package websail.clda.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import websail.clda.data.CannotConstraint;
import websail.clda.data.Constraint;
import websail.clda.data.ConstraintSet;
import websail.clda.data.Document;
import websail.clda.data.MustConstraint;
import websail.clda.util.Utility;
import cc.mallet.types.Dirichlet;

public class Estimator {
	//define a few parameters, set by heuristics
	static final int INNER_LOOP = 10;
	static final int GLOBE_CONCENTRATE = 1000;
	static final int MUST_CONCENTRATE = 1000;
	static final int CANNOT_CONCENTRATE = 1000;
	
	public static void estimate(LDA lda, int iteration, ConstraintSet cset){
		boolean hasConstraint = false;
		List<Constraint> clist = null;
		if(cset != null){
			clist = new ArrayList<Constraint>(cset);
			System.out.println(clist.size());
			hasConstraint = true;
		}

		Random rng = new Random();
		for(int iter = 1; iter <= iteration; iter++){
			if(iter%100 == 0)
				System.out.println("running estimation " + iter + " iterations");

			HashMap<Integer, Constraint> constraints = new HashMap<Integer, Constraint>();
			if(hasConstraint){
				for(int t = 0 ; t < INNER_LOOP; t++){
					Constraint constraint =  clist.get(rng.nextInt(clist.size()));
					constraints.put(constraint.target, constraint);
				}
			}
			for(int index = 0; index < lda.data.size(); index++){
				if(constraints.keySet().contains(index) ){
					updateAlpha(lda, constraints.get(index));
				}
				Document doc = lda.data.get(index);
				docEstimation(lda, doc, index);
			}
		}
		lda.computeTheta();
	}
	
	public static void updateAlpha(LDA lda, Constraint constraint){
		int target = constraint.target;
		double[] old_theta = lda.compute_theta(target);
		double[] new_theta = new double[lda.K];
		
		if(constraint instanceof MustConstraint){
			int pair = (Integer)constraint.getField().get("similar");
			double[] pair_theta = lda.compute_theta(pair);
			new_theta = ml_average(lda, old_theta, pair_theta);
			//update alpha given new theta
			for(int k = 0 ; k < lda.K; k++)
				lda.alpha[target][k] = GLOBE_CONCENTRATE * new_theta[k]; 

		}
		if(constraint instanceof CannotConstraint){
			int pair = (Integer)constraint.getField().get("dissimilar");
			double[] pair_theta = lda.compute_theta(pair);
			new_theta = cl_average(lda, old_theta, pair_theta);
			//update alpha given new theta
			for(int k = 0 ; k < lda.K; k++)
				lda.alpha[target][k] = GLOBE_CONCENTRATE * new_theta[k]; 
		}
		
	}
	
	private static double[] cl_average(LDA lda, double[] old_theta, double[] theta){
		double[] alpha = new double[lda.K];
		double sum = 0.0;
		for(int k = 0; k < lda.K; k++){
			alpha[k] = 1.0 - theta[k];
			sum += alpha[k];
		}
		for(int k = 0; k < lda.K; k++){
			alpha[k] /= sum;
			alpha[k] = alpha[k] * CANNOT_CONCENTRATE + 0.0000001;
		}
		Dirichlet dirichlet = new Dirichlet(alpha);
		double[] new_theta = dirichlet.nextDistribution();
		double[] average_theta = new double[lda.K];
		for(int k = 0 ; k < lda.K; k++){
			average_theta[k] = (old_theta[k] + new_theta[k]) / 2.0;
		}
		new_theta = max(old_theta, average_theta, theta);
		return new_theta;
	}
	
	public static double[] max(double[] old_theta, double[] average_theta, double[] theta){
		double old_distance = Utility.klDivergence(old_theta, theta);
		double new_distance = Utility.klDivergence(average_theta, theta);
		if (old_distance > new_distance)
			return old_theta;
		else
			return average_theta;
	}
	/*new theta position is in the average of old_theta and pair_theta*/
	private static double[] ml_average(LDA lda, double[] old_theta, double[] theta) {
		double[] alpha = new double[lda.K];
		for(int k = 0; k < lda.K; k++)
			alpha[k] = MUST_CONCENTRATE * theta[k] + 0.0000001; // alpha[k] must be a positive
		Dirichlet dirichlet = new Dirichlet(alpha);
		double[] new_theta = dirichlet.nextDistribution();
		for(int k = 0; k < lda.K; k++)
			new_theta[k] = (old_theta[k] + new_theta[k]) / 2.0;
		return new_theta;
	}
	
	public static void constraintEstimation(Constraint constraint){
		if(constraint instanceof MustConstraint){
			//docEstimation()
		}
		
	}
	
	public static void docEstimation(LDA lda, Document doc, int index){
		int[] words = doc.words;
		int[] topics = doc.topics;
		
		for(int widx = 0 ; widx < words.length; widx++){
			int w = words[widx];
			int t = topics[widx];
			int new_t = doSampling(lda, index, w, t);
			doc.topics[widx] = new_t;
		}
	}
	
	public static int doSampling(LDA lda, int d, int w, int t){
		lda.nwt[w][t]--; lda.ndt[d][t]--; lda.nt[t]--;

		double[] p = new double[lda.K];	
		for (int k = 0; k < lda.K; k++){
			p[k] = (lda.nwt[w][k] + lda.beta)/(lda.nt[k] + lda.W * lda.beta) *
					(lda.ndt[d][k] + lda.alpha[d][k]);
		}
		int new_t = Utility.sample(p, lda.rng);

		lda.nwt[w][new_t]++; lda.ndt[d][new_t]++; lda.nt[new_t]++;
		return new_t;
	}
}
