package websail.clda.model;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import websail.clda.util.Pair;

/**
 * this class prints out LDA top keywords for each topic
 *
 */

public class LDATopWords {

	public static void print(LDA lda, int numTWords, boolean rank){
		ArrayList<ArrayList<String>> result = getTWords(lda, numTWords, rank);
		for(int k = 0; k < lda.K; k++){
			ArrayList<String> twords_per_topic = result.get(k);
			System.out.print("topic " + k + ": ");
			for(String word : twords_per_topic){
				System.out.print(word + " ");
			}
			System.out.println();
		}
	}
	
	public static ArrayList<ArrayList<String>> getTWords(LDA lda, int numTWords, boolean rank){
		ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>(); 
		int[][] twords = getTwordsIndex(lda, numTWords, rank);
		for(int k = 0 ; k < lda.K; k++){
			ArrayList<String> twords_per_topic = new ArrayList<String>();
			for(int i = 0 ; i < numTWords; i++){
				String word = lda.data.vocab[twords[k][i]];
				twords_per_topic.add(word);
			}
			result.add(twords_per_topic);
		}
		return result;
	}
	
	public static int[][] getTwordsIndex(LDA lda, int numTWords, boolean rank){
		int[][] twords = new int[lda.K][numTWords];
		lda.computePhi();
		double[] tempsum = new double[lda.W];
		for(int w = 0 ; w < lda.W; w++){
			for(int k = 0 ; k < lda.K; k++){
				tempsum[w] += lda.phi[k][w];
			}
		}	
		for (int k = 0; k < lda.K; k++){//for each topic
			List<Pair> wordsProbsList = new ArrayList<Pair>(); 
			for (int w = 0; w < lda.W; w++){//for each word
				double newscore = lda.phi[k][w];
				if(rank)
					newscore = reRankKeywordMethod(lda, k, w, tempsum);
				Pair p = new Pair(w, newscore, false);	
				wordsProbsList.add(p);
			}
			Collections.sort(wordsProbsList);				
			for (int i = 0; i < Math.min(numTWords, lda.W); i++){
				int index = (Integer)wordsProbsList.get(i).first;
				twords[k][i] = index;
			}
		} 	
		return twords;
	}
	
	private static double reRankKeywordMethod(LDA lda, int k, int w, double[] tempsum){
		return lda.phi[k][w] / tempsum[w];
	}
}