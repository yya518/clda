package websail.clda.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

public class Utility {
	
	public static void print(double[][] matrix){
		for(int i = 0; i < matrix.length; i++)
			System.out.println(Arrays.toString(matrix[i]));
	}
	
	/**
	 * normalize a probability distribution
	 */
	public static void normalize(double[] p){
		double sum = 0.0;
		for(int i = 0; i < p.length; i++)
			sum += p[i];
		if(sum != 0)
			for(int i = 0; i < p.length; i++)
				p[i] /= sum;
	}
	
	/**
	 * compute cosine similarity of two vectors 
	 */
	public static double cosineSimilary(double[] a, double[] b){
		double dotProduct = 0.0;
	    double normA = 0.0;
	    double normB = 0.0;
	    for (int i = 0; i < a.length; i++) {
	        dotProduct += a[i] * b[i];
	        normA += Math.pow(a[i], 2);
	        normB += Math.pow(b[i], 2);
	    }   
	    return dotProduct / (Math.sqrt(normA) * Math.sqrt(normB));
    }
	
	/**
	 * compute KL Divergence of two probability distributions, p1, p2
	 * note: which one is p and which one is q is important.
	 * it is the measure of information lost when using q to approximate p
	 */
	public static double klDivergence(double[] p, double[] q){
		final double log2 = Math.log(2);
		double klDiv = 0.0;
		for (int i = 0; i < p.length; ++i) {
			if (p[i] == 0.0) 
				continue;
			if (q[i] == 0.0) 
				continue; 

			klDiv += p[i] * Math.log( p[i] / q[i] );
		}
		return klDiv / log2;
	}
	
	public static double symmetricKL(double[] p, double[] q){
		return klDivergence(p,q) + klDivergence(q,p);
	}
	
	//get a sample index from distribution p
	public static int sample(double[] p, Random rng){
		for (int k = 1; k < p.length; k++)
			p[k] += p[k - 1];
		double u = rng.nextDouble() * p[p.length - 1];
		int new_t = 0;
		for (; new_t < p.length; new_t++){
			if ( u < p[new_t]) //sample topic w.r.t distribution p
				break;
		}
		return new_t;
	}
	
	public static int maxElementIndex(double[] p){
		int index = 0;
		double max = 0.0;
		for(int i = 0; i < p.length; i++){
			if(p[i] > max){
				index = i;
				max = p[i];
			}
		}
		return index;
	}
	
	/**
	 * Dump Collection to file, one item per line
	 */
	public static <T> void writeLines(Collection<T> values, String outname)throws IOException{
		FileWriter out = new FileWriter(new File(outname));
		for(T val : values){				
			out.write(val + "\n");
		}
		out.close();
	}
	
	public static List<String> readLines(InputStream is){
		ArrayList<String> retval = new ArrayList<String>();
		BufferedReader in = 
				new BufferedReader(new InputStreamReader(is));
		String curLine;
		try {
			curLine = in.readLine();
			while(curLine != null){
				retval.add(curLine.trim());
				curLine = in.readLine();
			}
			return retval;
		}catch (IOException ioe)
		{
			System.out.println(String.format("Bad file(name): %s\n", 
					ioe.toString()));
			return null;
		}
	}
	
	/**
	 * Just return each line of the file as String
	 * 
	 * @param filename
	 * @return
	 */
	public static List<String> readLines(String filename)
	{
		ArrayList<String> retval = new ArrayList<String>();
		try
		{
			BufferedReader in = 
					new BufferedReader(new FileReader(filename));
			String curLine = in.readLine();
			while(curLine != null)
			{
				retval.add(curLine.trim());
				curLine = in.readLine();
			}
			return retval;
		}
		catch (IOException ioe)
		{
			System.out.println(String.format("Bad file(name): %s\n", 
					ioe.toString()));
			return null;
		}
	}
	
	/**
	 * list all files (except hidden files and subdirectories) under a directory
	 * @param dirname
	 * @return
	 */
	public static ArrayList<String> listfiles(String dirname){
		ArrayList<String> filenames= new ArrayList<String>();
		File[] files = new File(dirname).listFiles();
		for (File file : files){
			if(!file.isHidden() && file.isFile()){
				filenames.add(file.toString());
			}
		}
		return filenames;
	}

	/**
	 * given a directory, list all (only) subdirectories
	 * @param dirname
	 * @return
	 */
	public static ArrayList<String> findSubdirectories(String dirname){
		ArrayList<String> dirnames = new ArrayList<String>();
		File[] directories = new File(dirname).listFiles();
		for (File dir : directories){
			if(dir.isDirectory()){
				dirnames.add(dir.toString());
			}
		}
		return dirnames;
	}
	

}
