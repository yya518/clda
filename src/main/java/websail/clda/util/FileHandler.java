package websail.clda.util;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;

public class FileHandler {
	public static <T> T load(String f, Class<T> cls) throws IOException{
		Gson gson = new Gson();
		BufferedReader br = new BufferedReader(new FileReader(f));
		T data = gson.fromJson(br, cls);
		return data;
	}

	public static <T> void save(String f, T obj) throws IOException{
		Gson gson = new Gson();
		String json = gson.toJson(obj);
		FileWriter writer = new FileWriter(new File(f));
		writer.write(json);
		writer.close();
	}
}

