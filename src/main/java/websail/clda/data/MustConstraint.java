package websail.clda.data;

import java.util.HashMap;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.common.primitives.Ints;

public class MustConstraint extends Constraint{
	int similar;
	public MustConstraint(){
		
	}
	public MustConstraint(int target, int pair) {
		super(target);
		this.similar = pair;
	}
	@Override
	public void fromJson(String str) {
		JSONParser parser = new JSONParser();

		JSONObject obj;
		try {
			obj = (JSONObject) parser.parse(str);
			this.target = Ints.checkedCast( (Long) obj.get("target") );
			this.similar = Ints.checkedCast( (Long) obj.get("similar") );
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		obj.put("target", this.target);
		obj.put("similar", this.similar);
		return obj;
	}
	
	@Override
	public String toString() {
		return MustConstraint.class.getSimpleName() + " " + this.target + " " + this.similar;
	}
	@Override
	public HashMap<String, Object> getField() {
		HashMap<String, Object> fields = new HashMap<String, Object>();
		fields.put("similar", this.similar);
		return fields;
	}
}
