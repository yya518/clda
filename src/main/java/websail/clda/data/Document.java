package websail.clda.data;

import java.util.Arrays;

public class Document {
	public int index;
	public String text;
	public String category;
	public String path;
	public String time;
	public int[] words;
	public int[] topics;
	public double[] theta;
	
	public Document(int index){
		this.index = index;
	}
	public void setIndex(int index){
		this.index = index;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public void setPath(String path) {
		this.path = path;
	}
	public void setTheta(double[] theta) {
		this.theta = theta;
	}
	public void setTime(String time){
		this.time = time;
	}
	
	public void setText(String text){
		this.text = text;
	}

	public void setWords(int[] words) {
		this.words = words;
	}	
	@Override
	public String toString(){
		return "Document [idx=" + index + ", category=" + category + ", text=" 
				+ text + ", time=" + time 
				+ ", words=" + printWords(words) + "]";
	}
	
	
	public static String printWords(int[] ws){
		String print = "";
		if(ws!= null && ws.length >=3)
			print += "["+ ws[0] + ", " + ws[1] + ", " + ws[2] +", ...]";
		else
			print = Arrays.toString(ws);
		return print;
	}
}