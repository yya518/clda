package websail.clda.data;

import java.util.HashMap;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.common.primitives.Ints;


public class CannotConstraint extends Constraint{
	int dissimilar;
	
	public CannotConstraint(){}
	
	public CannotConstraint(int target, int pair) {
		super(target);
		this.dissimilar = pair;
	}

	@Override
	public void fromJson(String str) {
		JSONParser parser = new JSONParser();

		JSONObject obj;
		try {
			obj = (JSONObject) parser.parse(str);
			this.target = Ints.checkedCast( (Long) obj.get("target") );
			this.dissimilar = Ints.checkedCast( (Long) obj.get("dissimilar") );
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		obj.put("target", this.target);
		obj.put("dissimilar", this.dissimilar);
		return obj;
	}

	@Override
	public String toString() {
		return CannotConstraint.class.getSimpleName() + " " + this.target + " " + this.dissimilar;
	}

	@Override
	public HashMap<String, Object> getField() {
		HashMap<String, Object> fields = new HashMap<String, Object>();
		fields.put("dissimilar", this.dissimilar);
		return fields;
	}

}