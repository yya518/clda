package websail.clda.data;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import websail.clda.data.Dataset;

public class ConstraintReader {

	/*load constraint set from *.constraint.json*/
	public static ConstraintSet loadConstraint(String cfile, Dataset data) throws IOException{
		if(cfile == null)
			return null;
		ConstraintSet cset = new ConstraintSet();

		JSONParser parser = new JSONParser();
		try {
			JSONObject jsonObject = (JSONObject) parser.parse(new FileReader(cfile));

			
			//read mustlink
			JSONArray mustlink = (JSONArray) jsonObject.get("mustlink");
			for(Object str : mustlink){
				Constraint cs = new MustConstraint();
				cs.fromJson(str.toString());
				cset.add(cs);
			}
			
			//read cannotlink
			JSONArray cannotlink = (JSONArray) jsonObject.get("cannotlink");
			for(Object str : cannotlink){
				Constraint cs = new CannotConstraint();
				cs.fromJson(str.toString());
				cset.add(cs);
			}
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return cset;
	}
}

