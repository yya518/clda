package websail.clda.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class Dataset {
	public Document[] documents;
	public String[] vocab;
	
	public Dataset(){
	
	}
	public Dataset(Document[] documents, String[] vocab){
		this.documents = documents;
		this.vocab = vocab;
	}
	
	public int size(){
		return documents.length;
	}
	
	public Document get(int index){
		return this.documents[index];
	}
	
	public void setDocuments(Document[] documents){
		this.documents = documents;
	}
	
	public void setVocab(String[] vocab) {
		this.vocab = vocab;
	}

	public int vocabSize() {
		return vocab.length;
	}
	
	//return a list of categories the dataset contains
	public HashMap<String, Integer> getCategories(){
		HashMap<String, Integer> temp = new HashMap<String, Integer>();
		for(Document doc : this.documents){
			if( !temp.containsKey(doc.category) )
				temp.put(doc.category, temp.size());
		}
		return temp;
	}
	
}
