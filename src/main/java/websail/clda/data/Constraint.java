package websail.clda.data;

import java.util.HashMap;

import org.json.simple.JSONObject;

//import websail.core.LDA;
import websail.clda.data.Document;

public abstract class Constraint {
	public int target;
	
	public Constraint(){
		
	}
	
	public Constraint(int target){
		this.target = target;
	}
	
	//define loss function on topic t for constraint
	//public abstract double penalty(int t, LDA lda);
		
	public abstract JSONObject toJson();
	
	public abstract void fromJson(String str);
	
	public abstract HashMap<String, Object> getField();
}
