package websail.clda.data;

import java.io.IOException;
import java.util.Random;

import websail.clda.util.FileHandler;

public class ConstraintGenerator {

	public static ConstraintSet generate(Dataset data, int size){
		ConstraintSet cSet = new ConstraintSet();
		Random rng = new Random();
		for(int i = 0 ; i < size; i++){
			int a = rng.nextInt(data.size());
			int b = rng.nextInt(data.size());
			if(data.documents[a].category.equals(data.documents[b].category)){
				MustConstraint mc = new MustConstraint(a, b);
				cSet.add(mc);
			}else{
				CannotConstraint cc = new CannotConstraint(a, b);
				cSet.add(cc);
			}
			
		}
		return cSet;
	}
	
	/*3newsgroup for now*/
	public static void main(String[] args) throws IOException{
		Dataset data = FileHandler.load("/home/yi/workspace/userLDA/userLDA-resources/3news/3news.data", Dataset.class);
		
		ConstraintSet cset = ConstraintGenerator.generate(data, 50000);
		
		cset.save("/home/yi/workspace/userLDA/userLDA-resources/3news/3news.constraint.json");
	}
}
