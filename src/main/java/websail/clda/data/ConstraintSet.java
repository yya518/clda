package websail.clda.data;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


public class ConstraintSet extends HashSet<Constraint>{
	
	//each target document is associated with a list of constraints
	public HashMap<Integer, List<Constraint>> invertedIndex(){
		HashMap<Integer, List<Constraint>> inverted = new HashMap<Integer, List<Constraint>>();
		
		for(Constraint cs : this){
			int docIndex = cs.target;
			if( inverted.containsKey(docIndex) ){
				inverted.get(docIndex).add(cs);
			} else{
				List<Constraint> constraints = new ArrayList<Constraint>();
				constraints.add(cs);
				inverted.put(docIndex, constraints);
			}
		}
		
		return inverted;
	}
	
	public HashSet<Integer> getAllTargetIndex(){
		HashSet<Integer> indexes = new HashSet<Integer>();
		for(Constraint cs : this)
			indexes.add(cs.target);
		return indexes;
	}

	/*
	 * save to json file
	 */
	@SuppressWarnings("unchecked")
	public void save(String file) throws IOException{
		JSONObject obj = new JSONObject();
		JSONArray seedlist = new JSONArray();
		JSONArray mustlist = new JSONArray();
		JSONArray cannotlist = new JSONArray();
		JSONArray relativelist = new JSONArray();
		FileWriter out = new FileWriter(file);
		for(Constraint cs : this){
			if (cs instanceof MustConstraint)
				mustlist.add(cs.toJson());
			else if (cs instanceof CannotConstraint)
				cannotlist.add(cs.toJson());
		}
		obj.put("mustlink", mustlist);
		obj.put("cannotlink", cannotlist);
		out.write(obj.toJSONString());
		out.close();
	}
}
